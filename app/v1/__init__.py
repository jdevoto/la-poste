from flask import Blueprint, request
from flask_cors import CORS

from app.models.letter import Letter
from app.models.update import add_update_letter, update_all_letters

from threading import Thread

v1 = Blueprint("v1", __name__)
CORS(v1)

@v1.route('/ping', methods=['GET'])
def ep_ping():
    return "pong", 200


@v1.route('/letters', methods=['POST'])
def ep_setup_create_letter():
    # Example of ORM usage (SQLAlchemy)
    letter = Letter()
    letter.add()
    return f"All done : letter object {letter.id} has been created", 200

@v1.route('/fetch-letter/<tracking>', methods=['GET'])
def fetch_single_letter(tracking):
    r, code = add_update_letter(tracking)

    ret = {"status": code}
    return ret, r 

@v1.route('/fetch-all-letters')
def fetch_all_letters():
    letterdb = Letter()
    all_letters = letterdb.query.all()

    # Return current state of status
    ret = {}
    for letter in all_letters:
        ret[letter.tracking_number] = letter.status 

    # Update with new statuses in database
    thread = Thread(target=update_all_letters)
    thread.daemon = True
    thread.start()

    return ret, 200
