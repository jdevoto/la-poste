from app import db

from app.models.letter import Letter
from app.config import config

import requests
import json

# Calls La Poste suivi API for a given tracking number
# Updates the database or adds new entry if it doesn't exist
# Returns request status code and the shipment status code of the tracking.
# Code is none if shipment is not found or there is an error.
def add_update_letter(tracking):
    headers = {
            "content-type": "application/json",
            "X-Okapi-Key": config["API_KEY"]
    }
    r = requests.get("%s/%s" % (config["API_URL"], tracking), headers=headers) 
    code = None

    if r.status_code == 200:
        letterdb = Letter()
        qletter = letterdb.query.filter_by(tracking_number=tracking).first()

        events = r.json()["shipment"]["event"]
        if len(events) > 0:
            code = events[0]["code"]

        if qletter is None:
            new = Letter(tracking_number=tracking,status=code)
            db.session.add(new)
        else:
            qletter.status = code
        db.session.commit()

    return r.status_code, code

# To be run in a separate thread
def update_all_letters():
    letterdb = Letter()
    all_letters = letterdb.query.all()

    for letter in all_letters:
        add_update_letter(letter.tracking_number)

