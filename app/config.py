import os

basedir = os.path.abspath(os.path.dirname(__file__))

CONFIG_FILE = basedir + "/config"

class Config:
    SQLALCHEMY_DATABASE_URI = "sqlite:///test.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    ENV_TYPE = "development"


class ProductionConfig(Config):
    ENV_TYPE = "production"

config = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
    "default": DevelopmentConfig,
}

# Read config file which will hold API key.
# Ideally would be much more secured.
with open(CONFIG_FILE, "r") as f:
    lines = f.read().split('\n')
for line in lines:
    tok = line.split('=')
    if len(tok) < 2:
        continue
    key = tok[0].strip()
    value = tok[1].strip()
    config[key] = value

