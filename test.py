import requests
import json

URL = "http://127.0.0.1:5000/v1"

print("Testing ping")
r = requests.get("%s/ping" % URL)
assert r.status_code == 200

print("Testing invalid fetch id=3")
r = requests.get("%s/fetch-letter/x" % URL)
assert r.status_code == 404

print("Testing fetch id=3C00638102084")
r = requests.get("%s/fetch-letter/3C00638102084" % URL)
assert r.status_code == 404
assert r.json()["status"] == None

print("Testing fetch id=4P36275770836")
r = requests.get("%s/fetch-letter/4P36275770836" % URL)
assert r.status_code == 200
assert r.json()["status"] == "PC1"

print("Testing fetch all")
r = requests.get("%s/fetch-all-letters" % URL)
assert r.status_code == 200
assert r.json() == { 
  "3C00638101810": None, 
  "3C00638101995": None, 
  "3C00638102022": None, 
  "3C00638102046": None, 
  "3C00638102084": None, 
  "4P36275770836": "PC1"
}



